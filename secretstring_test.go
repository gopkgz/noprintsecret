package noprintsecret_test

import (
	"fmt"
	"testing"

	"gitlab.com/gopkgz/noprintsecret"
)

func TestSecretString(t *testing.T) {
	cases := []struct {
		name          string
		input         string
		expectedValue string
		expectedPrint string
	}{
		{
			name:          "empty",
			input:         "",
			expectedValue: "",
			expectedPrint: "********",
		},
		{
			name:          "non-empty",
			input:         "1234567890",
			expectedValue: "1234567890",
			expectedPrint: "********",
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			s := noprintsecret.String(c.input)
			if s.Value() != c.expectedValue {
				t.Errorf("expected %q, got %q", c.expectedValue, s.Value())
			}
			if s.String() != c.expectedPrint {
				t.Errorf("expected %q, got %q", c.expectedPrint, s.String())
			}
			if fmt.Sprintf("%s!%v", s, s) != c.expectedPrint+"!"+c.expectedPrint {
				t.Errorf("expected %q, got %q", c.expectedPrint+"!"+c.expectedPrint, fmt.Sprintf("%s!%v", s, s))
			}
		})
	}
}
