package noprintsecret

type String string

func (s String) String() string {
	return "********"
}

func (s String) Value() string {
	return string(s)
}
